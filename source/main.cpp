/////////////////////////////////////////////////////////////
// CINEMA 4D SDK                                           //
/////////////////////////////////////////////////////////////
// (c) 1989-2004 MAXON Computer GmbH, all rights reserved  //
/////////////////////////////////////////////////////////////

// Starts the plugin registration

#include "c4d.h"
#include <iostream>


// forward declarations
Bool RegisterMyTagPlugin(void);


Bool PluginStart(void)
{
	// tag / expression plugins
	if (!RegisterMyTagPlugin()) return FALSE;
	
	GePrint("======================================");
	GePrint("SyncMovieFrame V2.21 loaded successfully.");
	GePrint("(c) 2013 by Peter's Raw Materials, all rights reserved");
    GePrint("www.peterkienle.com - this plugin is freeware");
	GePrint("======================================");

	return TRUE;
}


void PluginEnd(void)
{
}



Bool PluginMessage(LONG id, void *data)
{
	//use the following lines to set a plugin priority
	//
	switch (id)
	{
		case C4DPL_INIT_SYS:
			if (!resource.Init()) return TRUE; // don't start plugin without resource


			// important, the serial hook must be registered before PluginStart(), best in C4DPL_INIT_SYS
			//if (!RegisterExampleSNHook()) return FALSE;
			
			return TRUE;

		case C4DMSG_PRIORITY: 
			return TRUE;

	}

	return FALSE;
}
