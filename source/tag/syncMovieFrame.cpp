/////////////////////////////////////////////////////////////
// CINEMA 4D SDK                                           //
/////////////////////////////////////////////////////////////
// (c) 1989-2004 MAXON Computer GmbH, all rights reserved  //
/////////////////////////////////////////////////////////////

// SyncMovieFrame plugin tag
// version 2.01
// 5/9/11


#include "c4d.h"
#include "c4d_symbols.h"
#include "Tsyncmovieframe.h"

#include "lib_description.h"
#include "customgui_priority.h"

// be sure to use a unique ID obtained from www.plugincafe.com
#define ID_MYTAGPLUGIN	1026862

// Help path
#define HelpPath GeGetPluginPath() + "help"


class MyTagPlugin : public TagData
{
	public:
		virtual Bool Init(GeListNode *node);
        virtual Bool Message(GeListNode* node, LONG type, void* data);
		virtual EXECUTIONRESULT Execute(BaseTag *tag, BaseDocument *doc, BaseObject *op, BaseThread *bt, LONG priority, EXECUTIONFLAGS flags);		
		static NodeData *Alloc(void) { return gNew MyTagPlugin; }
};



Bool MyTagPlugin::Init(GeListNode *node)
{
	BaseTag			*tag  = (BaseTag*)node;
	BaseContainer	*data = tag->GetDataInstance();

	data->SetBool(POWER_SWITCH,TRUE);
	
	GeData d;
	if (node->GetParameter(DescLevel(EXPRESSION_PRIORITY),d,DESCFLAGS_GET_0))
	{
		PriorityData *pd = (PriorityData*)d.GetCustomDataType(CUSTOMGUI_PRIORITY_DATA);
		if (pd) pd->SetPriorityValue(PRIORITYVALUE_CAMERADEPENDENT,GeData(TRUE));
		node->SetParameter(DescLevel(EXPRESSION_PRIORITY),d,DESCFLAGS_SET_0);
	}

	return TRUE;
}


Bool MyTagPlugin::Message(GeListNode *node, LONG type, void *data)
{
    //GePrint("AxisLockTag::Message");
    DescriptionCommand *dc = (DescriptionCommand*) data;
    
    if (type == MSG_DESCRIPTION_COMMAND)
    {
        switch (dc->id[0].id) {
                
            case HELP_BUTTON:
                if (GeFExist(HelpPath, TRUE))
                {
                    GeExecuteFile(HelpPath + "index.html");
                } else {
                    GeOutString(GeLoadString(IDS_HELP_ERROR), GEMB_OK);
                }
                break;
                
        }
    }
    
    return true;
}

EXECUTIONRESULT MyTagPlugin::Execute(BaseTag *tag, BaseDocument *doc, BaseObject *op, BaseThread *bt, LONG priority, EXECUTIONFLAGS flags)
{
	
	// this is the bypass switch - if off just exit
	if (!tag->GetData().GetBool(POWER_SWITCH)) return EXECUTIONRESULT_OK;


	BaseTag *myTag=op->GetFirstTag();
	
	while (myTag)												// loop until we find texture tag or run out of tags
	{
		
		if (myTag->IsInstanceOf(Ttexture)) break;				// found the first Texture Tag
		myTag = myTag->GetNext();								// no - on to next tag					
	}
	if(!myTag) return EXECUTIONRESULT_OK;						// didn't find a Texture Tag - exit
	
	TextureTag *myTextureTag = (TextureTag*)myTag;				// typecast from BaseTag to TextureTag
	BaseMaterial *myMaterial = myTextureTag->GetMaterial();		// get pointer to the Material
	
	myMaterial->Update(TRUE, TRUE);								// update the Material does the trick
	
	return EXECUTIONRESULT_OK;
}




Bool RegisterMyTagPlugin(void)
{
	// may not need this line
	String name=GeLoadString(IDS_MYTAG); if (!name.Content()) return TRUE;
	
	return RegisterTagPlugin(ID_MYTAGPLUGIN,GeLoadString(IDS_MYTAG),TAG_EXPRESSION|TAG_VISIBLE,MyTagPlugin::Alloc,"Tsyncmovieframe",AutoBitmap("syncmovieframe.tif"),0);
}
